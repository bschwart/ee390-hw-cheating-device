set SynModuleInfo {
  {SRCNAME hwcheatp2_Pipeline_VITIS_LOOP_26_1 MODELNAME hwcheatp2_Pipeline_VITIS_LOOP_26_1 RTLNAME hwcheatp2_hwcheatp2_Pipeline_VITIS_LOOP_26_1
    SUBMODULES {
      {MODELNAME hwcheatp2_flow_control_loop_pipe_sequential_init RTLNAME hwcheatp2_flow_control_loop_pipe_sequential_init BINDTYPE interface TYPE internal_upc_flow_control INSTNAME hwcheatp2_flow_control_loop_pipe_sequential_init_U}
    }
  }
  {SRCNAME hwcheatp2 MODELNAME hwcheatp2 RTLNAME hwcheatp2 IS_TOP 1
    SUBMODULES {
      {MODELNAME hwcheatp2_gmem_m_axi RTLNAME hwcheatp2_gmem_m_axi BINDTYPE interface TYPE adapter IMPL m_axi}
      {MODELNAME hwcheatp2_control_s_axi RTLNAME hwcheatp2_control_s_axi BINDTYPE interface TYPE interface_s_axilite}
    }
  }
}
