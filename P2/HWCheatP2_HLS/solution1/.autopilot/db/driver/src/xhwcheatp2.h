// ==============================================================
// Vitis HLS - High-Level Synthesis from C, C++ and OpenCL v2022.2 (64-bit)
// Tool Version Limit: 2019.12
// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// ==============================================================
#ifndef XHWCHEATP2_H
#define XHWCHEATP2_H

#ifdef __cplusplus
extern "C" {
#endif

/***************************** Include Files *********************************/
#ifndef __linux__
#include "xil_types.h"
#include "xil_assert.h"
#include "xstatus.h"
#include "xil_io.h"
#else
#include <stdint.h>
#include <assert.h>
#include <dirent.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>
#include <stddef.h>
#endif
#include "xhwcheatp2_hw.h"

/**************************** Type Definitions ******************************/
#ifdef __linux__
typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
#else
typedef struct {
    u16 DeviceId;
    u64 Control_BaseAddress;
} XHwcheatp2_Config;
#endif

typedef struct {
    u64 Control_BaseAddress;
    u32 IsReady;
} XHwcheatp2;

typedef u32 word_type;

/***************** Macros (Inline Functions) Definitions *********************/
#ifndef __linux__
#define XHwcheatp2_WriteReg(BaseAddress, RegOffset, Data) \
    Xil_Out32((BaseAddress) + (RegOffset), (u32)(Data))
#define XHwcheatp2_ReadReg(BaseAddress, RegOffset) \
    Xil_In32((BaseAddress) + (RegOffset))
#else
#define XHwcheatp2_WriteReg(BaseAddress, RegOffset, Data) \
    *(volatile u32*)((BaseAddress) + (RegOffset)) = (u32)(Data)
#define XHwcheatp2_ReadReg(BaseAddress, RegOffset) \
    *(volatile u32*)((BaseAddress) + (RegOffset))

#define Xil_AssertVoid(expr)    assert(expr)
#define Xil_AssertNonvoid(expr) assert(expr)

#define XST_SUCCESS             0
#define XST_DEVICE_NOT_FOUND    2
#define XST_OPEN_DEVICE_FAILED  3
#define XIL_COMPONENT_IS_READY  1
#endif

/************************** Function Prototypes *****************************/
#ifndef __linux__
int XHwcheatp2_Initialize(XHwcheatp2 *InstancePtr, u16 DeviceId);
XHwcheatp2_Config* XHwcheatp2_LookupConfig(u16 DeviceId);
int XHwcheatp2_CfgInitialize(XHwcheatp2 *InstancePtr, XHwcheatp2_Config *ConfigPtr);
#else
int XHwcheatp2_Initialize(XHwcheatp2 *InstancePtr, const char* InstanceName);
int XHwcheatp2_Release(XHwcheatp2 *InstancePtr);
#endif

void XHwcheatp2_Start(XHwcheatp2 *InstancePtr);
u32 XHwcheatp2_IsDone(XHwcheatp2 *InstancePtr);
u32 XHwcheatp2_IsIdle(XHwcheatp2 *InstancePtr);
u32 XHwcheatp2_IsReady(XHwcheatp2 *InstancePtr);
void XHwcheatp2_EnableAutoRestart(XHwcheatp2 *InstancePtr);
void XHwcheatp2_DisableAutoRestart(XHwcheatp2 *InstancePtr);
u32 XHwcheatp2_Get_return(XHwcheatp2 *InstancePtr);

void XHwcheatp2_Set_inputList(XHwcheatp2 *InstancePtr, u64 Data);
u64 XHwcheatp2_Get_inputList(XHwcheatp2 *InstancePtr);
void XHwcheatp2_Set_outputList(XHwcheatp2 *InstancePtr, u64 Data);
u64 XHwcheatp2_Get_outputList(XHwcheatp2 *InstancePtr);
void XHwcheatp2_Set_maxLength(XHwcheatp2 *InstancePtr, u32 Data);
u32 XHwcheatp2_Get_maxLength(XHwcheatp2 *InstancePtr);
void XHwcheatp2_Set_searchVal(XHwcheatp2 *InstancePtr, u32 Data);
u32 XHwcheatp2_Get_searchVal(XHwcheatp2 *InstancePtr);

void XHwcheatp2_InterruptGlobalEnable(XHwcheatp2 *InstancePtr);
void XHwcheatp2_InterruptGlobalDisable(XHwcheatp2 *InstancePtr);
void XHwcheatp2_InterruptEnable(XHwcheatp2 *InstancePtr, u32 Mask);
void XHwcheatp2_InterruptDisable(XHwcheatp2 *InstancePtr, u32 Mask);
void XHwcheatp2_InterruptClear(XHwcheatp2 *InstancePtr, u32 Mask);
u32 XHwcheatp2_InterruptGetEnabled(XHwcheatp2 *InstancePtr);
u32 XHwcheatp2_InterruptGetStatus(XHwcheatp2 *InstancePtr);

#ifdef __cplusplus
}
#endif

#endif
