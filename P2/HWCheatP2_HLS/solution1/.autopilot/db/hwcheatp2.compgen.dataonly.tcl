# This script segment is generated automatically by AutoPilot

set axilite_register_dict [dict create]
set port_control {
ap_start { }
ap_done { }
ap_ready { }
ap_idle { }
ap_return { 
	dir o
	width 32
	depth 1
	mode ap_ctrl_hs
	offset 16
	offset_end 0
}
inputList { 
	dir I
	width 64
	depth 1
	mode ap_none
	offset 24
	offset_end 35
}
outputList { 
	dir I
	width 64
	depth 1
	mode ap_none
	offset 36
	offset_end 47
}
maxLength { 
	dir I
	width 32
	depth 1
	mode ap_none
	offset 48
	offset_end 55
}
searchVal { 
	dir I
	width 32
	depth 1
	mode ap_none
	offset 56
	offset_end 63
}
interrupt {
}
}
dict set axilite_register_dict control $port_control


