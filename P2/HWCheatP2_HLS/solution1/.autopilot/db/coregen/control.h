// 0x00 : Control signals
//        bit 0  - ap_start (Read/Write/COH)
//        bit 1  - ap_done (Read/COR)
//        bit 2  - ap_idle (Read)
//        bit 3  - ap_ready (Read/COR)
//        bit 7  - auto_restart (Read/Write)
//        bit 9  - interrupt (Read)
//        others - reserved
// 0x04 : Global Interrupt Enable Register
//        bit 0  - Global Interrupt Enable (Read/Write)
//        others - reserved
// 0x08 : IP Interrupt Enable Register (Read/Write)
//        bit 0 - enable ap_done interrupt (Read/Write)
//        bit 1 - enable ap_ready interrupt (Read/Write)
//        others - reserved
// 0x0c : IP Interrupt Status Register (Read/TOW)
//        bit 0 - ap_done (Read/TOW)
//        bit 1 - ap_ready (Read/TOW)
//        others - reserved
// 0x10 : Data signal of ap_return
//        bit 31~0 - ap_return[31:0] (Read)
// 0x18 : Data signal of inputList
//        bit 31~0 - inputList[31:0] (Read/Write)
// 0x1c : Data signal of inputList
//        bit 31~0 - inputList[63:32] (Read/Write)
// 0x20 : reserved
// 0x24 : Data signal of outputList
//        bit 31~0 - outputList[31:0] (Read/Write)
// 0x28 : Data signal of outputList
//        bit 31~0 - outputList[63:32] (Read/Write)
// 0x2c : reserved
// 0x30 : Data signal of maxLength
//        bit 31~0 - maxLength[31:0] (Read/Write)
// 0x34 : reserved
// 0x38 : Data signal of searchVal
//        bit 31~0 - searchVal[31:0] (Read/Write)
// 0x3c : reserved
// (SC = Self Clear, COR = Clear on Read, TOW = Toggle on Write, COH = Clear on Handshake)

#define CONTROL_ADDR_AP_CTRL         0x00
#define CONTROL_ADDR_GIE             0x04
#define CONTROL_ADDR_IER             0x08
#define CONTROL_ADDR_ISR             0x0c
#define CONTROL_ADDR_AP_RETURN       0x10
#define CONTROL_BITS_AP_RETURN       32
#define CONTROL_ADDR_INPUTLIST_DATA  0x18
#define CONTROL_BITS_INPUTLIST_DATA  64
#define CONTROL_ADDR_OUTPUTLIST_DATA 0x24
#define CONTROL_BITS_OUTPUTLIST_DATA 64
#define CONTROL_ADDR_MAXLENGTH_DATA  0x30
#define CONTROL_BITS_MAXLENGTH_DATA  32
#define CONTROL_ADDR_SEARCHVAL_DATA  0x38
#define CONTROL_BITS_SEARCHVAL_DATA  32
