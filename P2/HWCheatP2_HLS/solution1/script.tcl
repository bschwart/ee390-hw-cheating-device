############################################################
## This file is generated automatically by Vitis HLS.
## Please DO NOT edit it.
## Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
############################################################
open_project HWCheatP2_HLS
set_top hwcheatp2
add_files HLS/hwcheatp2.cpp
add_files HLS/hwcheatp2.h
add_files -tb HLS/main.cpp
open_solution "solution1" -flow_target vivado
set_part {xc7z010iclg225-1L}
create_clock -period 10 -name default
#source "./HWCheatP2_HLS/solution1/directives.tcl"
csim_design
csynth_design
cosim_design
export_design -rtl verilog -format ip_catalog
