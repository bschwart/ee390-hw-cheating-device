// ==============================================================
// Vitis HLS - High-Level Synthesis from C, C++ and OpenCL v2022.2 (64-bit)
// Tool Version Limit: 2019.12
// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// ==============================================================
/***************************** Include Files *********************************/
#include "xhwcheatp2.h"

/************************** Function Implementation *************************/
#ifndef __linux__
int XHwcheatp2_CfgInitialize(XHwcheatp2 *InstancePtr, XHwcheatp2_Config *ConfigPtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(ConfigPtr != NULL);

    InstancePtr->Control_BaseAddress = ConfigPtr->Control_BaseAddress;
    InstancePtr->IsReady = XIL_COMPONENT_IS_READY;

    return XST_SUCCESS;
}
#endif

void XHwcheatp2_Start(XHwcheatp2 *InstancePtr) {
    u32 Data;

    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XHwcheatp2_ReadReg(InstancePtr->Control_BaseAddress, XHWCHEATP2_CONTROL_ADDR_AP_CTRL) & 0x80;
    XHwcheatp2_WriteReg(InstancePtr->Control_BaseAddress, XHWCHEATP2_CONTROL_ADDR_AP_CTRL, Data | 0x01);
}

u32 XHwcheatp2_IsDone(XHwcheatp2 *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XHwcheatp2_ReadReg(InstancePtr->Control_BaseAddress, XHWCHEATP2_CONTROL_ADDR_AP_CTRL);
    return (Data >> 1) & 0x1;
}

u32 XHwcheatp2_IsIdle(XHwcheatp2 *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XHwcheatp2_ReadReg(InstancePtr->Control_BaseAddress, XHWCHEATP2_CONTROL_ADDR_AP_CTRL);
    return (Data >> 2) & 0x1;
}

u32 XHwcheatp2_IsReady(XHwcheatp2 *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XHwcheatp2_ReadReg(InstancePtr->Control_BaseAddress, XHWCHEATP2_CONTROL_ADDR_AP_CTRL);
    // check ap_start to see if the pcore is ready for next input
    return !(Data & 0x1);
}

void XHwcheatp2_EnableAutoRestart(XHwcheatp2 *InstancePtr) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XHwcheatp2_WriteReg(InstancePtr->Control_BaseAddress, XHWCHEATP2_CONTROL_ADDR_AP_CTRL, 0x80);
}

void XHwcheatp2_DisableAutoRestart(XHwcheatp2 *InstancePtr) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XHwcheatp2_WriteReg(InstancePtr->Control_BaseAddress, XHWCHEATP2_CONTROL_ADDR_AP_CTRL, 0);
}

u32 XHwcheatp2_Get_return(XHwcheatp2 *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XHwcheatp2_ReadReg(InstancePtr->Control_BaseAddress, XHWCHEATP2_CONTROL_ADDR_AP_RETURN);
    return Data;
}
void XHwcheatp2_Set_inputList(XHwcheatp2 *InstancePtr, u64 Data) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XHwcheatp2_WriteReg(InstancePtr->Control_BaseAddress, XHWCHEATP2_CONTROL_ADDR_INPUTLIST_DATA, (u32)(Data));
    XHwcheatp2_WriteReg(InstancePtr->Control_BaseAddress, XHWCHEATP2_CONTROL_ADDR_INPUTLIST_DATA + 4, (u32)(Data >> 32));
}

u64 XHwcheatp2_Get_inputList(XHwcheatp2 *InstancePtr) {
    u64 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XHwcheatp2_ReadReg(InstancePtr->Control_BaseAddress, XHWCHEATP2_CONTROL_ADDR_INPUTLIST_DATA);
    Data += (u64)XHwcheatp2_ReadReg(InstancePtr->Control_BaseAddress, XHWCHEATP2_CONTROL_ADDR_INPUTLIST_DATA + 4) << 32;
    return Data;
}

void XHwcheatp2_Set_outputList(XHwcheatp2 *InstancePtr, u64 Data) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XHwcheatp2_WriteReg(InstancePtr->Control_BaseAddress, XHWCHEATP2_CONTROL_ADDR_OUTPUTLIST_DATA, (u32)(Data));
    XHwcheatp2_WriteReg(InstancePtr->Control_BaseAddress, XHWCHEATP2_CONTROL_ADDR_OUTPUTLIST_DATA + 4, (u32)(Data >> 32));
}

u64 XHwcheatp2_Get_outputList(XHwcheatp2 *InstancePtr) {
    u64 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XHwcheatp2_ReadReg(InstancePtr->Control_BaseAddress, XHWCHEATP2_CONTROL_ADDR_OUTPUTLIST_DATA);
    Data += (u64)XHwcheatp2_ReadReg(InstancePtr->Control_BaseAddress, XHWCHEATP2_CONTROL_ADDR_OUTPUTLIST_DATA + 4) << 32;
    return Data;
}

void XHwcheatp2_Set_maxLength(XHwcheatp2 *InstancePtr, u32 Data) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XHwcheatp2_WriteReg(InstancePtr->Control_BaseAddress, XHWCHEATP2_CONTROL_ADDR_MAXLENGTH_DATA, Data);
}

u32 XHwcheatp2_Get_maxLength(XHwcheatp2 *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XHwcheatp2_ReadReg(InstancePtr->Control_BaseAddress, XHWCHEATP2_CONTROL_ADDR_MAXLENGTH_DATA);
    return Data;
}

void XHwcheatp2_Set_searchVal(XHwcheatp2 *InstancePtr, u32 Data) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XHwcheatp2_WriteReg(InstancePtr->Control_BaseAddress, XHWCHEATP2_CONTROL_ADDR_SEARCHVAL_DATA, Data);
}

u32 XHwcheatp2_Get_searchVal(XHwcheatp2 *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XHwcheatp2_ReadReg(InstancePtr->Control_BaseAddress, XHWCHEATP2_CONTROL_ADDR_SEARCHVAL_DATA);
    return Data;
}

void XHwcheatp2_InterruptGlobalEnable(XHwcheatp2 *InstancePtr) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XHwcheatp2_WriteReg(InstancePtr->Control_BaseAddress, XHWCHEATP2_CONTROL_ADDR_GIE, 1);
}

void XHwcheatp2_InterruptGlobalDisable(XHwcheatp2 *InstancePtr) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XHwcheatp2_WriteReg(InstancePtr->Control_BaseAddress, XHWCHEATP2_CONTROL_ADDR_GIE, 0);
}

void XHwcheatp2_InterruptEnable(XHwcheatp2 *InstancePtr, u32 Mask) {
    u32 Register;

    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Register =  XHwcheatp2_ReadReg(InstancePtr->Control_BaseAddress, XHWCHEATP2_CONTROL_ADDR_IER);
    XHwcheatp2_WriteReg(InstancePtr->Control_BaseAddress, XHWCHEATP2_CONTROL_ADDR_IER, Register | Mask);
}

void XHwcheatp2_InterruptDisable(XHwcheatp2 *InstancePtr, u32 Mask) {
    u32 Register;

    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Register =  XHwcheatp2_ReadReg(InstancePtr->Control_BaseAddress, XHWCHEATP2_CONTROL_ADDR_IER);
    XHwcheatp2_WriteReg(InstancePtr->Control_BaseAddress, XHWCHEATP2_CONTROL_ADDR_IER, Register & (~Mask));
}

void XHwcheatp2_InterruptClear(XHwcheatp2 *InstancePtr, u32 Mask) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XHwcheatp2_WriteReg(InstancePtr->Control_BaseAddress, XHWCHEATP2_CONTROL_ADDR_ISR, Mask);
}

u32 XHwcheatp2_InterruptGetEnabled(XHwcheatp2 *InstancePtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    return XHwcheatp2_ReadReg(InstancePtr->Control_BaseAddress, XHWCHEATP2_CONTROL_ADDR_IER);
}

u32 XHwcheatp2_InterruptGetStatus(XHwcheatp2 *InstancePtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    return XHwcheatp2_ReadReg(InstancePtr->Control_BaseAddress, XHWCHEATP2_CONTROL_ADDR_ISR);
}

