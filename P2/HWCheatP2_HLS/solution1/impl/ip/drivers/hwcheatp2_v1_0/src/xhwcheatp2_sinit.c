// ==============================================================
// Vitis HLS - High-Level Synthesis from C, C++ and OpenCL v2022.2 (64-bit)
// Tool Version Limit: 2019.12
// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// ==============================================================
#ifndef __linux__

#include "xstatus.h"
#include "xparameters.h"
#include "xhwcheatp2.h"

extern XHwcheatp2_Config XHwcheatp2_ConfigTable[];

XHwcheatp2_Config *XHwcheatp2_LookupConfig(u16 DeviceId) {
	XHwcheatp2_Config *ConfigPtr = NULL;

	int Index;

	for (Index = 0; Index < XPAR_XHWCHEATP2_NUM_INSTANCES; Index++) {
		if (XHwcheatp2_ConfigTable[Index].DeviceId == DeviceId) {
			ConfigPtr = &XHwcheatp2_ConfigTable[Index];
			break;
		}
	}

	return ConfigPtr;
}

int XHwcheatp2_Initialize(XHwcheatp2 *InstancePtr, u16 DeviceId) {
	XHwcheatp2_Config *ConfigPtr;

	Xil_AssertNonvoid(InstancePtr != NULL);

	ConfigPtr = XHwcheatp2_LookupConfig(DeviceId);
	if (ConfigPtr == NULL) {
		InstancePtr->IsReady = 0;
		return (XST_DEVICE_NOT_FOUND);
	}

	return XHwcheatp2_CfgInitialize(InstancePtr, ConfigPtr);
}

#endif

