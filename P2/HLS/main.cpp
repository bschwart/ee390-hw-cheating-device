#include <ap_int.h>
#include <stdint.h>
#include <stdio.h>
#include "hwcheatp2.h"

#define MAX_SIZE 500000

ap_uint<32> *inputList = (ap_uint<32>*) malloc(sizeof(int) * MAX_SIZE);
ap_uint<32> *output1_hw = (ap_uint<32>*) malloc(sizeof(int) * MAX_SIZE);
ap_uint<32> *output2_hw = (ap_uint<32>*) malloc(sizeof(int) * MAX_SIZE);
ap_uint<32> *output1_sw = (ap_uint<32>*) malloc(sizeof(int) * MAX_SIZE);
ap_uint<32> *output2_sw = (ap_uint<32>*) malloc(sizeof(int) * MAX_SIZE);

int main(int, char **)
{
	for (int i = 0; i < MAX_SIZE; ++i) {
		inputList[i] = i % 10;
	}

	// Testing HW

	int res1 = hwcheatp2(inputList, output1_hw, MAX_SIZE, 5);
	int res2 = hwcheatp2(inputList, output2_hw, MAX_SIZE, 6);
	printf("Result 1 HW: %d\n", res1);
	printf("Result 2 HW: %d\n", res2);

	// Testing SW

	ap_uint<32> * curAddr = inputList;
	int sum1 = 0;
	while (curAddr < (inputList + MAX_SIZE)) {
		if ((*curAddr) == 5) {
			output1_sw[sum1] = curAddr;
			sum1++;
		}
		curAddr = curAddr + 1;
	}
	printf("Result 1 SW: %d\n", sum1);

	curAddr = inputList;
	int sum2 = 0;
	while (curAddr < (inputList + MAX_SIZE)) {
		if ((*curAddr) == 6) {
			output2_sw[sum2] = curAddr;
			sum2++;
		}
		curAddr = curAddr + 1;
	}
	printf("Result 2 SW: %d\n", sum2);

	return 0;
}
