// Create a second peripheral that, given a list of memory addresses, produces a new list with all
// the memory addresses in the first list that contain the searched value. This peripheral receives as
// parameters: the physical start address of the input list, the physical start address of the output list,
// the maximum length of both lists (the same), and the value being searched. It will return the number
// of addresses stored in the output list.

// The application will use this peripheral as many times as necessary to reduce the list of candidates to one.
// The application will use only two lists, alternating their function as “input” or “output”
// in every invocation of the peripheral.

#include <ap_int.h>
#include <stdint.h>

int hwcheatp2(ap_uint<32> *inputList, ap_uint<32> *outputList,
			ap_uint<32> maxLength, ap_uint<32> searchVal)
{
#pragma HLS INTERFACE s_axilite port=maxLength
#pragma HLS INTERFACE s_axilite port=searchVal
#pragma HLS INTERFACE s_axilite port=return
#pragma HLS INTERFACE m_axi port=inputList offset=slave
#pragma HLS INTERFACE m_axi port=outputList offset=slave

	ap_uint<32> sum = 0;
	ap_uint<32> *curAddr = inputList;

	while (curAddr < inputList + maxLength) {
		if ((*curAddr) == searchVal) {
			outputList[sum] = curAddr;
			sum++;
		}
		curAddr = curAddr + 1;
	}
	return sum;
}
