#include <ap_int.h>
#include <stdint.h>
#include <stdio.h>
#include "hwcheatp1.h"

#define MAX_SIZE 500000

ap_uint<32> *memory = (ap_uint<32>*) malloc(sizeof(int) * MAX_SIZE);
ap_uint<32> *result1_hw = (ap_uint<32>*) malloc(sizeof(int) * MAX_SIZE);
ap_uint<32> *result2_hw = (ap_uint<32>*) malloc(sizeof(int) * MAX_SIZE);
ap_uint<32> *result1_sw = (ap_uint<32>*) malloc(sizeof(int) * MAX_SIZE);
ap_uint<32> *result2_sw = (ap_uint<32>*) malloc(sizeof(int) * MAX_SIZE);

int main(int, char **)
{
	for (int i = 0; i < MAX_SIZE; ++i) {
		memory[i] = i % 10;
	}

	// Testing HW

	int res1 = hwcheatp1(memory, memory + MAX_SIZE, result1_hw, MAX_SIZE, 5);
	int res2 = hwcheatp1(memory, memory + MAX_SIZE, result2_hw, MAX_SIZE, 6);
	printf("Result 1 HW: %d\n", res1);
	printf("Result 2 HW: %d\n", res2);

	// Testing SW

	ap_uint<32> * curAddr = memory;
	int sum1 = 0;
	while (curAddr < (memory + MAX_SIZE)) {
		if ((*curAddr) == 5) {
			result1_sw[sum1] = curAddr;
			sum1++;
		}
		curAddr = curAddr + 1;
	}
	printf("Result 1 SW: %d\n", sum1);

	curAddr = memory;
	int sum2 = 0;
	while (curAddr < (memory + MAX_SIZE)) {
		if ((*curAddr) == 6) {
			result2_sw[sum2] = curAddr;
			sum2++;
		}
		curAddr = curAddr + 1;
	}
	printf("Result 2 SW: %d\n", sum2);

	return 0;
}
