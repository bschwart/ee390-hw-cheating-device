// Given a starting and an ending memory address, produces a list of those addresses that contain a given value.
// The peripheral takes as parameters: the start address (1 048 576),
// 		The end address (536 870 908), a physical address to store the list, a maximum size for the list of
//		addresses (524 288), and the searched value. It will return the number of addresses stored in the list.
//The application will use this peripheral once, to generate the initial list of candidates.

#include <ap_int.h>
#include <stdint.h>

int hwcheatp1(ap_uint<32> *startAddr, ap_uint<32> *endAddr,
			ap_uint<32> *result, ap_uint<32> maxSize, ap_uint<32> searchVal)
{
#pragma HLS INTERFACE s_axilite port=maxSize
#pragma HLS INTERFACE s_axilite port=searchVal
#pragma HLS INTERFACE s_axilite port=return
#pragma HLS INTERFACE m_axi port=startAddr offset=slave
#pragma HLS INTERFACE m_axi port=endAddr offset=slave
#pragma HLS INTERFACE m_axi port=result offset=slave

	ap_uint<32> sum = 0;
	ap_uint<32> *curAddr = startAddr;

	while (curAddr < endAddr) {
		if ((*curAddr) == searchVal) {
			result[sum] = curAddr;
			sum++;
		}
		curAddr = curAddr + 1;
	}
	return sum;
}
