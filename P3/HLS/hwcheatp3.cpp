//Create a third peripheral that checks the value stored in a memory position and, if it is not the
//desired one, updates it. This peripheral takes as input a memory address, the desired value, and a
//counter value. After checking and possibly updating the memory position, the peripheral will wait
//for the specified number of cycles. The peripheral will have a register that the software can use to
//know how many times the variable has been modified.
//The cheating application can program this peripheral using a value of 1 ms as waiting period.
//When the user desires to stop cheating, the application will stop the peripheral

#include <ap_int.h>
#include <stdint.h>


void hwcheatp3(ap_uint<32> *addr, ap_uint<32> val, ap_uint<32> counter, ap_uint<32> numMods)
{
#pragma HLS INTERFACE s_axilite port=val
#pragma HLS INTERFACE s_axilite port=counter
#pragma HLS INTERFACE s_axilite port=numMods
#pragma HLS INTERFACE s_axilite port=return
#pragma HLS INTERFACE m_axi port=addr offset=slave

	if ((*addr) != val) {
		(*addr) = val;
		numMods++;
	}
	// Wait for number of cycles
	for (int i = 0; i < counter * 1000; ++i) {
		// do nothing
	}

}
