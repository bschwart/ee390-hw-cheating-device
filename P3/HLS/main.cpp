#include <ap_int.h>
#include <stdint.h>
#include <stdio.h>
#include "hwcheatp3.h"

#define MAX_SIZE 500000

ap_uint<32> *inputList = (ap_uint<32>*) malloc(sizeof(int) * MAX_SIZE);

int main(int, char **)
{
	for (int i = 0; i < MAX_SIZE; ++i) {
		inputList[i] = i % 10;
	}

	// Testing HW

	printf("Value at address mem[4] = %u\n", inputList[4]);
	printf("Starting cheating\n");

	for (int i = 0; i < 20; ++i) {
		hwcheatp3((inputList + 4), 99, 1000, 0);
		printf("Value at address mem[4] = %u\n", inputList[4]);
		printf("i = %d\n", i);
	}
	printf("Stopping cheating\n");
	//	printf("Number of updates HW: %d\n", mods);

	return 0;
}
