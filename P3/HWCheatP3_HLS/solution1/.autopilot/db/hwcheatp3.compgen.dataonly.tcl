# This script segment is generated automatically by AutoPilot

set axilite_register_dict [dict create]
set port_control {
addr { 
	dir I
	width 64
	depth 1
	mode ap_none
	offset 16
	offset_end 27
}
val_r { 
	dir I
	width 32
	depth 1
	mode ap_none
	offset 28
	offset_end 35
}
counter { 
	dir I
	width 32
	depth 1
	mode ap_none
	offset 36
	offset_end 43
}
}
dict set axilite_register_dict control $port_control


