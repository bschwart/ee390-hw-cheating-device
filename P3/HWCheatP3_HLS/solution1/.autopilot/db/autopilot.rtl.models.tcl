set SynModuleInfo {
  {SRCNAME hwcheatp3 MODELNAME hwcheatp3 RTLNAME hwcheatp3 IS_TOP 1
    SUBMODULES {
      {MODELNAME hwcheatp3_gmem_m_axi RTLNAME hwcheatp3_gmem_m_axi BINDTYPE interface TYPE adapter IMPL m_axi}
      {MODELNAME hwcheatp3_control_s_axi RTLNAME hwcheatp3_control_s_axi BINDTYPE interface TYPE interface_s_axilite}
    }
  }
}
