// ==============================================================
// Vitis HLS - High-Level Synthesis from C, C++ and OpenCL v2022.2 (64-bit)
// Tool Version Limit: 2019.12
// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// ==============================================================
#ifndef XHWCHEATP3_H
#define XHWCHEATP3_H

#ifdef __cplusplus
extern "C" {
#endif

/***************************** Include Files *********************************/
#ifndef __linux__
#include "xil_types.h"
#include "xil_assert.h"
#include "xstatus.h"
#include "xil_io.h"
#else
#include <stdint.h>
#include <assert.h>
#include <dirent.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>
#include <stddef.h>
#endif
#include "xhwcheatp3_hw.h"

/**************************** Type Definitions ******************************/
#ifdef __linux__
typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
#else
typedef struct {
    u16 DeviceId;
    u64 Control_BaseAddress;
} XHwcheatp3_Config;
#endif

typedef struct {
    u64 Control_BaseAddress;
    u32 IsReady;
} XHwcheatp3;

typedef u32 word_type;

/***************** Macros (Inline Functions) Definitions *********************/
#ifndef __linux__
#define XHwcheatp3_WriteReg(BaseAddress, RegOffset, Data) \
    Xil_Out32((BaseAddress) + (RegOffset), (u32)(Data))
#define XHwcheatp3_ReadReg(BaseAddress, RegOffset) \
    Xil_In32((BaseAddress) + (RegOffset))
#else
#define XHwcheatp3_WriteReg(BaseAddress, RegOffset, Data) \
    *(volatile u32*)((BaseAddress) + (RegOffset)) = (u32)(Data)
#define XHwcheatp3_ReadReg(BaseAddress, RegOffset) \
    *(volatile u32*)((BaseAddress) + (RegOffset))

#define Xil_AssertVoid(expr)    assert(expr)
#define Xil_AssertNonvoid(expr) assert(expr)

#define XST_SUCCESS             0
#define XST_DEVICE_NOT_FOUND    2
#define XST_OPEN_DEVICE_FAILED  3
#define XIL_COMPONENT_IS_READY  1
#endif

/************************** Function Prototypes *****************************/
#ifndef __linux__
int XHwcheatp3_Initialize(XHwcheatp3 *InstancePtr, u16 DeviceId);
XHwcheatp3_Config* XHwcheatp3_LookupConfig(u16 DeviceId);
int XHwcheatp3_CfgInitialize(XHwcheatp3 *InstancePtr, XHwcheatp3_Config *ConfigPtr);
#else
int XHwcheatp3_Initialize(XHwcheatp3 *InstancePtr, const char* InstanceName);
int XHwcheatp3_Release(XHwcheatp3 *InstancePtr);
#endif


void XHwcheatp3_Set_addr(XHwcheatp3 *InstancePtr, u64 Data);
u64 XHwcheatp3_Get_addr(XHwcheatp3 *InstancePtr);
void XHwcheatp3_Set_val_r(XHwcheatp3 *InstancePtr, u32 Data);
u32 XHwcheatp3_Get_val_r(XHwcheatp3 *InstancePtr);
void XHwcheatp3_Set_counter(XHwcheatp3 *InstancePtr, u32 Data);
u32 XHwcheatp3_Get_counter(XHwcheatp3 *InstancePtr);

#ifdef __cplusplus
}
#endif

#endif
