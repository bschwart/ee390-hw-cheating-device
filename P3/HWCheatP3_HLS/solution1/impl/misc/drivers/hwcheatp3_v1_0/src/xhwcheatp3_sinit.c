// ==============================================================
// Vitis HLS - High-Level Synthesis from C, C++ and OpenCL v2022.2 (64-bit)
// Tool Version Limit: 2019.12
// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// ==============================================================
#ifndef __linux__

#include "xstatus.h"
#include "xparameters.h"
#include "xhwcheatp3.h"

extern XHwcheatp3_Config XHwcheatp3_ConfigTable[];

XHwcheatp3_Config *XHwcheatp3_LookupConfig(u16 DeviceId) {
	XHwcheatp3_Config *ConfigPtr = NULL;

	int Index;

	for (Index = 0; Index < XPAR_XHWCHEATP3_NUM_INSTANCES; Index++) {
		if (XHwcheatp3_ConfigTable[Index].DeviceId == DeviceId) {
			ConfigPtr = &XHwcheatp3_ConfigTable[Index];
			break;
		}
	}

	return ConfigPtr;
}

int XHwcheatp3_Initialize(XHwcheatp3 *InstancePtr, u16 DeviceId) {
	XHwcheatp3_Config *ConfigPtr;

	Xil_AssertNonvoid(InstancePtr != NULL);

	ConfigPtr = XHwcheatp3_LookupConfig(DeviceId);
	if (ConfigPtr == NULL) {
		InstancePtr->IsReady = 0;
		return (XST_DEVICE_NOT_FOUND);
	}

	return XHwcheatp3_CfgInitialize(InstancePtr, ConfigPtr);
}

#endif

