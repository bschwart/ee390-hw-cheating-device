// ==============================================================
// Vitis HLS - High-Level Synthesis from C, C++ and OpenCL v2022.2 (64-bit)
// Tool Version Limit: 2019.12
// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// ==============================================================
// control
// 0x00 : reserved
// 0x04 : reserved
// 0x08 : reserved
// 0x0c : reserved
// 0x10 : Data signal of addr
//        bit 31~0 - addr[31:0] (Read/Write)
// 0x14 : Data signal of addr
//        bit 31~0 - addr[63:32] (Read/Write)
// 0x18 : reserved
// 0x1c : Data signal of val_r
//        bit 31~0 - val_r[31:0] (Read/Write)
// 0x20 : reserved
// 0x24 : Data signal of counter
//        bit 31~0 - counter[31:0] (Read/Write)
// 0x28 : reserved
// (SC = Self Clear, COR = Clear on Read, TOW = Toggle on Write, COH = Clear on Handshake)

#define XHWCHEATP3_CONTROL_ADDR_ADDR_DATA    0x10
#define XHWCHEATP3_CONTROL_BITS_ADDR_DATA    64
#define XHWCHEATP3_CONTROL_ADDR_VAL_R_DATA   0x1c
#define XHWCHEATP3_CONTROL_BITS_VAL_R_DATA   32
#define XHWCHEATP3_CONTROL_ADDR_COUNTER_DATA 0x24
#define XHWCHEATP3_CONTROL_BITS_COUNTER_DATA 32

