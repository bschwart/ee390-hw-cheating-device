// ==============================================================
// Vitis HLS - High-Level Synthesis from C, C++ and OpenCL v2022.2 (64-bit)
// Tool Version Limit: 2019.12
// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// ==============================================================
/***************************** Include Files *********************************/
#include "xhwcheatp3.h"

/************************** Function Implementation *************************/
#ifndef __linux__
int XHwcheatp3_CfgInitialize(XHwcheatp3 *InstancePtr, XHwcheatp3_Config *ConfigPtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(ConfigPtr != NULL);

    InstancePtr->Control_BaseAddress = ConfigPtr->Control_BaseAddress;
    InstancePtr->IsReady = XIL_COMPONENT_IS_READY;

    return XST_SUCCESS;
}
#endif

void XHwcheatp3_Set_addr(XHwcheatp3 *InstancePtr, u64 Data) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XHwcheatp3_WriteReg(InstancePtr->Control_BaseAddress, XHWCHEATP3_CONTROL_ADDR_ADDR_DATA, (u32)(Data));
    XHwcheatp3_WriteReg(InstancePtr->Control_BaseAddress, XHWCHEATP3_CONTROL_ADDR_ADDR_DATA + 4, (u32)(Data >> 32));
}

u64 XHwcheatp3_Get_addr(XHwcheatp3 *InstancePtr) {
    u64 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XHwcheatp3_ReadReg(InstancePtr->Control_BaseAddress, XHWCHEATP3_CONTROL_ADDR_ADDR_DATA);
    Data += (u64)XHwcheatp3_ReadReg(InstancePtr->Control_BaseAddress, XHWCHEATP3_CONTROL_ADDR_ADDR_DATA + 4) << 32;
    return Data;
}

void XHwcheatp3_Set_val_r(XHwcheatp3 *InstancePtr, u32 Data) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XHwcheatp3_WriteReg(InstancePtr->Control_BaseAddress, XHWCHEATP3_CONTROL_ADDR_VAL_R_DATA, Data);
}

u32 XHwcheatp3_Get_val_r(XHwcheatp3 *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XHwcheatp3_ReadReg(InstancePtr->Control_BaseAddress, XHWCHEATP3_CONTROL_ADDR_VAL_R_DATA);
    return Data;
}

void XHwcheatp3_Set_counter(XHwcheatp3 *InstancePtr, u32 Data) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XHwcheatp3_WriteReg(InstancePtr->Control_BaseAddress, XHWCHEATP3_CONTROL_ADDR_COUNTER_DATA, Data);
}

u32 XHwcheatp3_Get_counter(XHwcheatp3 *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XHwcheatp3_ReadReg(InstancePtr->Control_BaseAddress, XHWCHEATP3_CONTROL_ADDR_COUNTER_DATA);
    return Data;
}

