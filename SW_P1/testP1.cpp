#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>
extern "C" {
#include <libxlnk_cma.h>  // Required for memory-mapping functions from Xilinx
}

// Defines whether the vector memory is cacheable by the processors or not.
const uint32_t MEM_IS_CACHEABLE = 0;

///////////////////////////////////////
// Address constants
const uint32_t MAP_SIZE = 512*1024;
const uint32_t P1_ADDR = 0x40000000;
const uint32_t LENGTH = 500000; 

// Note: We divide the addresses of the registers by 4 because we use pointer arithmetic with uint32_t *

// These vectors are for testing the speed in SW - Not used with the accelerator.
// The reason is that the processor would normally use cacheable memory thru the virtual memory system, not the DMA allocated for the device.
uint32_t sw_startAddr[LENGTH], sw_result[LENGTH];

///////////////////////////////////////////////////////////////////////////////
unsigned long long CalcTimeDiff(const struct timespec & time2, const struct timespec & time1)
{
  return time2.tv_sec == time1.tv_sec ?
    time2.tv_nsec - time1.tv_nsec :
    (time2.tv_sec - time1.tv_sec - 1) * 1e9 + (1e9 - time1.tv_nsec) + time2.tv_nsec;
}

///////////////////////////////////////////////////////////////////////////////
bool CallAccel(volatile uint32_t * accelRegs, void * startAddr, void * endAddr, void * result, 
  uint32_t maxSize, uint32_t searchVal, unsigned long long & elapsed)
{
  // Offsets for the registers. Extracted from xargs_hw.c
  typedef enum {REG_CONTROL = 0, REG_RETURN = 0x10/4, REG_STARTADDR = 0x18/4,
				REG_ENDADDR = 0x24/4, REG_RESULT = 0x30/4, 
				REG_MAXSIZE = 0x3C/4, REG_SEARCHVAL = 0x44/4} TRegs;

  *(accelRegs + REG_STARTADDR) = (uint32_t)startAddr;
  *(accelRegs + REG_ENDADDR) = (uint32_t)endAddr;
  *(accelRegs + REG_RESULT) = (uint32_t)result;
  *(accelRegs + REG_MAXSIZE) = maxSize;
  *(accelRegs + REG_SEARCHVAL) = searchVal;

  printf("---Registers configured:---\n");
  printf("startAddr: %u\n", *(accelRegs + REG_STARTADDR));
  printf("endAddr: %u\n", *(accelRegs + REG_ENDADDR));
  printf("result: %u\n", *(accelRegs + REG_RESULT));
  printf("maxSize: %u\n", *(accelRegs + REG_MAXSIZE));
  printf("searchVal: %u\n", *(accelRegs + REG_SEARCHVAL));

  printf("Starting accel...\n"); 
  struct timespec start, end;
  clock_gettime(CLOCK_MONOTONIC_RAW, &start);
  uint32_t status;
  status = *(accelRegs + REG_CONTROL);
  status |= 1;  // We want to modify *exclusively* the start bit (bit 0, ap_start)
  *(accelRegs + REG_CONTROL) = status;

  do {
    status = *(accelRegs + REG_CONTROL);
  } while ( ( (status & 2) != 2)  ); // bit 1 is ap_done
  clock_gettime(CLOCK_MONOTONIC_RAW, &end);
  elapsed = CalcTimeDiff(end, start);

  // ap_done is clear-on-read (COR). Therefore, we got done the first time we read in the while. Now, we should get done=0, idle=1.
  status = *(accelRegs + REG_CONTROL);
  printf("Status: %u (should be idle. done already cleared)\n", status);
  
  uint32_t matches_found;
  matches_found = *(accelRegs + REG_RETURN);
  printf("Number of Finds HW: %u\n", matches_found);
  
  return true;
}

///////////////////////////////////////////////////////////////////////////////
int SoftwareProcessing(uint32_t * startAddr, uint32_t * endAddr, uint32_t * result, 
  uint32_t maxSize, uint32_t searchVal, unsigned long long & elapsed)
{
	struct timespec start, end;
	clock_gettime(CLOCK_MONOTONIC_RAW, &start);
	uint32_t sum = 0;
	uint32_t *curAddr = startAddr;

	while (curAddr < endAddr) {
		if ((*curAddr) == searchVal) {
			result[sum] = (uint32_t) curAddr;
			sum++;
		}
		curAddr = curAddr + 1;
	}
	clock_gettime(CLOCK_MONOTONIC_RAW, &end);
	elapsed = CalcTimeDiff(end, start);
	return sum;
}

///////////////////////////////////////////////////////////////////////
void InitMemory(uint32_t * memory, uint32_t maxSize)
{
	for (uint32_t i = 0; i < maxSize; ++i) {
		memory[i] = i % 10;
	}
}

///////////////////////////////////////////////////////////////////////////////
int main(int argc, char ** argv)
{
	volatile uint32_t * accelRegs;
	unsigned long long timeAccel, timeSW;
	
	uint32_t searchVal = 6;


	printf("\n\nThis program requires that the bitstream is loaded in the FPGA.\n");
	printf("This program has to be run with sudo.\n");
	printf("Press ENTER to confirm that the bitstream is loaded (proceeding without it can crash the board).\n\n");
	getchar();

	// Map the physical address of the adder into this app virtual address space
	accelRegs = (uint32_t*)cma_mmap(P1_ADDR, MAP_SIZE);
	if ((uint32_t)accelRegs == 0xFFFFFFFF) {
		printf("Error mapping the peripheral address (0x%08X)!\n", P1_ADDR);
		exit(-1);
	}
	printf("Address mapping done. Peripheral physical address 0x%08X mapped at 0x%08X\n", 
	P1_ADDR, (uint32_t)accelRegs);
	// We have to allocate DMA memory for the device. We receive addresses in the *virtual* address space of the application.
	// Then, we need to obtaiin the corresponding *physical* addresses that our peripheral can use on the bus.
	printf("Allocating DMA memory...\n");
	uint16_t * startAddr = (uint16_t *)cma_alloc(LENGTH * 4, MEM_IS_CACHEABLE);
	uint32_t * phyStartAddr = (uint32_t*)((uint32_t)cma_get_phy_addr(startAddr));
	uint16_t * endAddr = (startAddr + LENGTH*2 - 1);	// TODO: Work out why we multiply by 2
	uint32_t * phyEndAddr = (uint32_t*)((uint32_t)cma_get_phy_addr(endAddr));
	uint16_t * result = (uint16_t *)cma_alloc(LENGTH * 4, MEM_IS_CACHEABLE);
	uint32_t * phyResult = (uint32_t*)((uint32_t)cma_get_phy_addr(result));

	printf("DMA memory allocated.\n");
	printf("StartAddr: Virt: 0x%.8X (%u) // Phys: 0x%.8X (%u)\n", (uint32_t)startAddr, (uint32_t)startAddr, (uint32_t)phyStartAddr, (uint32_t)phyStartAddr);
	printf("EndAddr: Virt: 0x%.8X (%u) // Phys: 0x%.8X (%u)\n", (uint32_t)endAddr, (uint32_t)endAddr, (uint32_t)phyEndAddr, (uint32_t)phyEndAddr);
	printf("Result: Virt: 0x%.8X (%u) // Phys: 0x%.8X (%u)\n", (uint32_t)result, (uint32_t)result, (uint32_t)phyResult, (uint32_t)phyResult);

	if ( (startAddr == NULL) || (endAddr == NULL) || (result == NULL) ) {
		printf("Error allocating DMA memory for %u bytes.\n", LENGTH * 4);
	}
	else {
		printf("Initializing memory...\n");
		InitMemory((uint32_t *)startAddr, LENGTH);
		printf("Memory initialized...\n");

		// The peripheral works with physical addresses directly on the system bus.
		CallAccel(accelRegs, phyStartAddr, phyEndAddr, phyResult, 
				LENGTH, searchVal, timeAccel);

		//printf("Output Accelerator:\n");
		//printOutput(output);
		// printf("TEST OK!\n");


		// Measure the speed in SW.
		printf("Measuring speed in SW\n");
		InitMemory(sw_startAddr, LENGTH);
		int res;
		res = SoftwareProcessing(sw_startAddr, sw_startAddr + LENGTH, sw_result, LENGTH, searchVal, timeSW);
		printf("Number of Finds SW: %d", res);
		
		printf("\n\nTime accelerator: %0.3lf s (%llu ns)\n", timeAccel/1e9, timeAccel);
		printf("Time SW: %0.3lf s (%llu ns)\n", timeSW/1e9, timeSW);
		// Not sure how to access internal registers in Accelerator here...
		// printf("Number of Finds HW: %d", *(accelRegs + REG_RETURN);
	}

	// Free the DMA memory. ---IMPORTANT--- DMA memory is a system-wide resource!!!!!! It's not freed automatically when the app is closed.
	if (startAddr != NULL)
		cma_free(startAddr);
	if (endAddr != NULL)
		cma_free(endAddr);
	if (result != NULL)
		cma_free(result);


	// Unmap the physical address of the VectorAdder registers
	cma_munmap((void*)accelRegs, MAP_SIZE);

	return 0;
}


