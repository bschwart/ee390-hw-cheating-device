// Controlling application that interacts with the user to obtain the number of lives and
// uses the peripherals until the memory position is identified. The application has to allocate space
// for two lists in DMA-compatible memory, alternating them as input or output in each round. 



#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>
extern "C" {
#include <libxlnk_cma.h>  // Required for memory-mapping functions from Xilinx
}

// Defines whether the vector memory is cacheable by the processors or not.
const uint32_t MEM_IS_CACHEABLE = 0;

///////////////////////////////////////
// Address constants
const uint32_t MAP_SIZE = 512*1024;
const uint32_t P1_ADDR = 0x40000000;
const uint32_t P2_ADDR = 0x40010000;
const uint32_t P3_ADDR = 0x40020000;
const uint32_t P2_OFFSET = 0x10000;
const uint32_t P3_OFFSET = 0x20000;
const uint32_t MAXSIZE = 50000; 
void * START_ADDR = (void*) 1048576;
void * END_ADDR = (void*) 536870908;

// Note: We divide the addresses of the registers by 4 because we use pointer arithmetic with uint32_t *
///////////////////////////////////////////////////////////////////////////////
uint32_t CallP1(volatile uint32_t * accelRegs, void * startAddr, void * endAddr, void * result, 
	uint32_t maxSize, uint32_t searchVal) 
{
	typedef enum {REG_CONTROL = 0, REG_RETURN = 0x10/4, REG_STARTADDR = 0x18/4,
		REG_ENDADDR = 0x24/4, REG_RESULT = 0x30/4, 
		REG_MAXSIZE = 0x3C/4, REG_SEARCHVAL = 0x44/4} TRegs;
	*(accelRegs + REG_STARTADDR) = (uint32_t)startAddr;
	*(accelRegs + REG_ENDADDR) = (uint32_t)endAddr;
	*(accelRegs + REG_RESULT) = (uint32_t)result;
	*(accelRegs + REG_MAXSIZE) = maxSize;
	*(accelRegs + REG_SEARCHVAL) = searchVal;

	uint32_t status;
	status = *(accelRegs + REG_CONTROL);
	status |= 1;  // We want to modify *exclusively* the start bit (bit 0, ap_start)
	*(accelRegs + REG_CONTROL) = status;

	do {
		status = *(accelRegs + REG_CONTROL);
	} while ( ( (status & 2) != 2)  ); // bit 1 is ap_done


	uint32_t numAddresses = *(accelRegs + REG_RETURN);
	if (numAddresses == 1) {
		printf("List[0] = 0x%u\n", *(accelRegs + REG_RESULT));
	}
	return numAddresses;
}

uint32_t CallP2(volatile uint32_t * accelRegs, void * inputList, void * outputList, 
  	uint32_t maxLength, uint32_t searchVal)
{
	typedef enum {REG_CONTROL = 0, REG_RETURN = 0x10/4, 
		REG_INPUTLIST = 0x18/4,	REG_OUTPUTLIST = 0x24/4, 
		REG_MAXLENGTH = 0x30/4, REG_SEARCHVAL = 0x38/4} TRegs;
	*(accelRegs + P2_OFFSET + REG_INPUTLIST) = (uint32_t)inputList;
	*(accelRegs + P2_OFFSET + REG_OUTPUTLIST) = (uint32_t)outputList;
	*(accelRegs + P2_OFFSET + REG_MAXLENGTH) = maxLength;
	*(accelRegs + P2_OFFSET + REG_SEARCHVAL) = searchVal;

	uint32_t status;
	status = *(accelRegs + P2_OFFSET + REG_CONTROL);
	status |= 1;  // We want to modify *exclusively* the start bit (bit 0, ap_start)
	*(accelRegs + P2_OFFSET + REG_CONTROL) = status;

	do {
		status = *(accelRegs + P2_OFFSET + REG_CONTROL);
	} while ( ( (status & 2) != 2)  ); // bit 1 is ap_done

	// ap_done is clear-on-read (COR). Therefore, we got done the first time we read in the while. Now, we should get done=0, idle=1.
	uint32_t numAddresses = *(accelRegs + P2_OFFSET + REG_RETURN);
	if (numAddresses == 1) {
		printf("List[0] = 0x%u\n", *(accelRegs + P2_OFFSET + REG_OUTPUTLIST));
	}
	return numAddresses;
}

bool CallP3(volatile uint32_t * accelRegs, void * addr, uint32_t val, uint32_t counter)
{
	typedef enum {REG_CONTROL = 0, REG_ADDR = 0x10/4, 
		REG_VAL = 0x1C/4, REG_COUNTER = 0x24/4, 
		REG_NUMMODS = 0x2C/4} TRegs;

	*(accelRegs + P3_OFFSET + REG_ADDR) = (uint32_t)addr;
	*(accelRegs + P3_OFFSET + REG_VAL) = val;
	*(accelRegs + P3_OFFSET + REG_COUNTER) = counter;

	printf("Address: 0x%u\n", (uint32_t) addr);
	printf("RequiredValue: 0x%u (%d)\n", val, val);
	printf("IdleCounter: 0x%u (%dms)\n", counter, counter);
	printf("MemSetter started.\n");	

	// We want to modify the start bit (bit 0, ap_start) and the auto-restart bit (bit 7)
	uint32_t status;
	status = *(accelRegs + P3_OFFSET + REG_CONTROL);
	status |= 0x81; // 0b10000001  
	*(accelRegs + P3_OFFSET + REG_CONTROL) = status;


	// How do I stop execution when an ENTER is pressed?
	getchar();
	status ^= 0x80;
	*(accelRegs + P3_OFFSET + REG_CONTROL) = status;

	// while ( ( (status & 2) != 2)  ); 

	return true;
}

///////////////////////////////////////////////////////////////////////////////
int main(int argc, char ** argv)
{
	volatile uint32_t * accelRegs;

	printf("\n\nThis program requires that the bitstream is loaded in the FPGA.\n");
	printf("This program has to be run with sudo.\n");
	printf("Press ENTER to confirm that the bitstream is loaded (proceeding without it can crash the board).\n\n");
	getchar();

	// Map the physical address of the hw_cheat into this app virtual address space
	accelRegs = (uint32_t*)cma_mmap(P1_ADDR, MAP_SIZE);
	if ((uint32_t)accelRegs == 0xFFFFFFFF) {
		printf("Error mapping the peripheral address (0x%08X)!\n", P1_ADDR);
		exit(-1);
	}
	printf("Address mapping done. Peripheral 1 physical address 0x%08X mapped at 0x%08X\n", P1_ADDR, (uint32_t)accelRegs);
	printf("Address mapping done. Peripheral 2 physical address 0x%08X mapped at 0x%08X\n", P2_ADDR, (uint32_t)(accelRegs + P2_OFFSET));
	printf("Address mapping done. Peripheral 3 physical address 0x%08X mapped at 0x%08X\n", P3_ADDR, (uint32_t)(accelRegs + P3_OFFSET));

	// We have to allocate DMA memory for the device. We receive addresses in the *virtual* address space of the application.
	// Then, we need to obtaiin the corresponding *physical* addresses that our peripheral can use on the bus.
	printf("Allocating DMA memory...\n");
	uint16_t * list1 = (uint16_t *)cma_alloc(MAXSIZE * 4, MEM_IS_CACHEABLE);
	uint32_t * phyList1 = (uint32_t*)((uint32_t)cma_get_phy_addr(list1));
	uint16_t * list2 = (uint16_t *)cma_alloc(MAXSIZE * 4, MEM_IS_CACHEABLE);	
	uint32_t * phyList2 = (uint32_t*)((uint32_t)cma_get_phy_addr(list2));

	printf("DMA memory allocated.\n");
	printf("List1: Virt: 0x%.8X (%u) // Phys: 0x%.8X (%u)\n", (uint32_t)list1, (uint32_t)list1, (uint32_t)phyList1, (uint32_t)phyList1);
	printf("List2: Virt: 0x%.8X (%u) // Phys: 0x%.8X (%u)\n", (uint32_t)list2, (uint32_t)list2, (uint32_t)phyList2, (uint32_t)phyList2);

	if ( (list1 == NULL) || (list2 == NULL)) {
		printf("Error allocating DMA memory for %u bytes.\n", MAXSIZE * 4);
	}
	else {

		printf("Welcome to the cheating device\n");

		// Generate initial list of candidates
		printf("Enter the number of lives: ");
		uint32_t searchVal = getchar();
		uint32_t res = CallP1(accelRegs, START_ADDR, END_ADDR, phyList1, MAXSIZE, searchVal);
		printf("Number of memory positions found: %d\n", res);
		printf("\n");

		// list1 is initially input list
		bool list1_input = true;
		while (res != 1) {
			printf("Enter the number of lives: ");
			searchVal = getchar();
			printf("Searching... Do not lose any life!\n");

			// alternate lists
			if (list1_input) {
				res = CallP2(accelRegs, phyList1, phyList2, MAXSIZE, searchVal);
			}
			else {
				res = CallP2(accelRegs, phyList2, phyList1, MAXSIZE, searchVal);
			}
			// flip boolean value
			list1_input = !list1_input;

			printf("Number of memory positions found: %d\n", res);
			printf("\n");
		}

		// At this point, we've found the address
		printf("Found memory positions of lifes variable.\n");
		printf("Press ENTER to start cheating...\n\n");
		getchar();

		// How do I pass the final address to the third peripheral?

		// I was thinking: 
		// 		check which list ended with the final result
		//		if it's list1 for example, pass *(list1)
		void * addr;
		if (list1_input) {
			addr = (void *) list1;
		} else {
			addr = (void *) list2;
		}
		// Should the execution be here, or inside the CallP3 function?
		CallP3(accelRegs, addr, 99, 100);

	}

	// Free the DMA memory. ---IMPORTANT--- DMA memory is a system-wide resource!!!!!! It's not freed automatically when the app is closed.
	if (list1 != NULL)
		cma_free(list1);
	if (list2 != NULL)
		cma_free(list2);


	// Unmap the physical address of the HW_Cheating registers
	cma_munmap((void*)accelRegs, MAP_SIZE);

	return 0;
}


